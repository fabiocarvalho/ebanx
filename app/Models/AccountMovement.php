<?php
namespace App\Models;

use App\Models\Account;

class AccountMovement extends BaseModel
{
    protected $table = 'account_movement';

    public static function processAmount(int $account_id, float $amount, string $event_type)
    {
        $account = Account::getAccount($account_id);
        if ($account) {
            $account_amount = new AccountMovement();
            $account_amount->account_id = $account_id;
            $account_amount->amount = $amount;
            $account_amount->event_type = $event_type;
            $account_amount->save();
        }
    }

    public static function getBalance(int $account_id)
    {
        try {
            $account = Account::getAccount($account_id);
            if ($account) {
                $account_movement = AccountMovement::selectRaw("sum(account_movement.amount) as balance")
                ->where("account_movement.account_id", "=", $account_id)
                ->first();
                return [
                    'id' => (string)$account_id, //Converted to string because test (https://ipkiss.pragmazero.com) did not accept returning integer
                    'balance' => round($account_movement->balance)
                ];
            } else {
                throw new \Exception(404);
            }
        } catch (\Exception $e) {
            return [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }
    }

    public static function withdraw(int $account_id, float $amount)
    {
        try {
            $account = Account::getAccount($account_id);
            if ($account) {
                $amount = -abs($amount);
                self::processAmount($account_id, $amount, 'withdraw');
                return self::getBalance($account_id);
            } else {
                throw new \Exception(404);
            }
        } catch (\Exception $e) {
            return [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }
    }

    public static function transfer(int $account_id, float $amount, int $destination_id)
    {
        try {
            $account = Account::getAccount($account_id);
            if ($account) {
                $debit_amount = -abs($amount);
                self::processAmount($account_id, $debit_amount, 'transfer_debit');

                Account::createAccount($destination_id);
                AccountMovement::processAmount($destination_id, $amount, 'transfer_credit');

                return [
                    'origin' => self::getBalance($account_id),
                    'destination' => self::getBalance($destination_id)
                ];
            } else {
                throw new \Exception(404);
            }
        } catch (\Exception $e) {
            return [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }
    }
}
