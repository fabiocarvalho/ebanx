<?php
namespace App\Models;

use Illuminate\Http\Request;
use App\Models\AccountMovement;

class Account extends BaseModel
{
    protected $table = 'account';

    public static function resetAllData()
    {
        try {
            AccountMovement::query()->truncate();
            Account::query()->truncate();

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function createAccount(int $account_id)
    {
        $account = self::getAccount($account_id);
        if (!$account) {
            $account = new Account();
            $account->account_id = $account_id;
            $account->save();
        }
    }

    public static function getAccount(int $account_id)
    {
        $account = Account::selectRaw("account.*")
        ->where('account.account_id', '=', $account_id)
        ->first();

        return $account;
    }
}
