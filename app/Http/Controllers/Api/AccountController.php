<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\Account;
use App\Models\AccountMovement;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function reset(Request $request)
    {
        $result = Account::resetAllData();
        if ($result) {
            return response('OK', 200);
        } else {
            return response(null, 404);
        }
    }

    public function balance(Request $request)
    {
        $result = AccountMovement::getBalance((int)$request->get('account_id'));
        if (isset($result['status'])  && $result['status'] == 'error' && $result['message'] == 404) {
            return response(0, 404);
        } else {
            return response($result['balance'], 200);
        }
    }

    public function event(Request $request)
    {
        $content = json_decode($request->getContent(), true);

        if (isset($content['type'])) {
            switch ($content['type']) {
                case 'deposit':
                    Account::createAccount((int)$content['destination']);
                    AccountMovement::processAmount((int)$content['destination'], (float)$content['amount'], (string)$content['type']);

                    $result = AccountMovement::getBalance((int)$content['destination']);
                    $response = [
                        'data' => json_encode(['destination' => $result]),
                        'code' => 201
                    ];
                    break;
                case 'withdraw':
                    $result = AccountMovement::withdraw((int)$content['origin'], (float)$content['amount']);
                    if (isset($result['status'])  && $result['status'] == 'error' && $result['message'] == 404) {
                        $response = [
                            'data' => 0,
                            'code' => 404
                        ];
                    } else {
                        $response = [
                            'data' => json_encode(['origin' => $result]),
                            'code' => 201
                        ];
                    }
                    break;
                case 'transfer':
                    $result = AccountMovement::transfer((int)$content['origin'], (float)$content['amount'], (int)$content['destination']);
                    if (isset($result['status'])  && $result['status'] == 'error' && $result['message'] == 404) {
                        $response = [
                            'data' => 0,
                            'code' => 404
                        ];
                    } else {
                        $response = [
                            'data' => json_encode($result),
                            'code' => 201
                        ];
                    }
                    break;
                default:
                    $response = [
                        'data' => 0,
                        'code' => 404
                    ];
                    break;
            }
        } else {
            $response = [
                'data' => 0,
                'code' => 404
            ];
        }

        return response($response['data'], $response['code']);
    }
}
